import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_example/providers/riverpod_counter.dart';

final appCounterRiverpod = ChangeNotifierProvider<CounterRiverpod>(
  (ref) => CounterRiverpod(),
);

class CounterPage extends ConsumerStatefulWidget {
  const CounterPage({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _CounterPageState();
}

class _CounterPageState extends ConsumerState<CounterPage> {
  @override
  Widget build(BuildContext context) {
    final mediaquery = MediaQuery.of(context).size;
    var applyCount = ref.watch(appCounterRiverpod);
    final appName = ref.watch(appNameRiverpod);
    // final appName = ref.listen<String>(appNameRiverpod, (String? oldName, String newName){
    //   print('New Title $newName');
    // });
    return SafeArea(
      child: Scaffold(
        appBar: appBar(appName, context),
        floatingActionButton: rowfloatingActionButton(applyCount, mediaquery),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Text(
                "Value :  " + applyCount.value.toString(),
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(color: Colors.white),
              ),
            ),
            SizedBox(
              height: mediaquery.height * 0.02,
            ),
            Text(
              "Example about riverpod ",
              style: Theme.of(context)
                  .textTheme
                  .caption
                  ?.copyWith(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

  AppBar appBar(String appName, BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      title: Text(
        appName,
        style: Theme.of(context)
            .textTheme
            .headline5
            ?.copyWith(color: Colors.white),
      ),
    );
  }

  Widget rowfloatingActionButton(CounterRiverpod applyCount, Size mediaquery) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FloatingActionButton(
          onPressed: () {
            applyCount.add();
          },
          child: const Icon(Icons.add),
        ),
        SizedBox(
          width: mediaquery.width * 0.02,
        ),
        FloatingActionButton(
          onPressed: () {
            applyCount.subtract();
          },
          child: const Icon(Icons.remove),
        ),
      ],
    );
  }
}
